<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Subject extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function has_reads(): HasMany
    {
        return $this->hasMany(UserReadModule::class);
    }
    public function showSubject($id)
{
    $subject = Subject::find($id); // Assuming Subject is your model
    return view('subjects.show', compact('subject'));
}
}
